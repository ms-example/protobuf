# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [src/userspb/common.proto](#src_userspb_common-proto)
    - [UserData](#users-UserData)
  
- [src/userspb/internal.proto](#src_userspb_internal-proto)
    - [UserInfoInternalRequest](#users-UserInfoInternalRequest)
    - [UserInfoInternalResponse](#users-UserInfoInternalResponse)
  
    - [UsersInternalService](#users-UsersInternalService)
  
- [src/userspb/public.proto](#src_userspb_public-proto)
    - [ForgotPasswordRequest](#users-ForgotPasswordRequest)
    - [RefreshTokensRequest](#users-RefreshTokensRequest)
    - [RefreshTokensResponse](#users-RefreshTokensResponse)
    - [ResetPasswordRequest](#users-ResetPasswordRequest)
    - [SignInRequest](#users-SignInRequest)
    - [SignInResponse](#users-SignInResponse)
    - [SignUpRequest](#users-SignUpRequest)
    - [SignUpResponse](#users-SignUpResponse)
    - [UpdateUserRequest](#users-UpdateUserRequest)
    - [UserInfoRequest](#users-UserInfoRequest)
    - [UserInfoResponse](#users-UserInfoResponse)
  
    - [UsersPublicService](#users-UsersPublicService)
  
- [Scalar Value Types](#scalar-value-types)



<a name="src_userspb_common-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## src/userspb/common.proto



<a name="users-UserData"></a>

### UserData
User data


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_uuid | [string](#string) |  | User UUID |
| name | [string](#string) |  | User name |
| last_name | [string](#string) |  | User last name |
| email | [string](#string) |  | User email |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | Time of creation |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | Time of update |
| last_login | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | Time of last login |





 

 

 

 



<a name="src_userspb_internal-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## src/userspb/internal.proto



<a name="users-UserInfoInternalRequest"></a>

### UserInfoInternalRequest
Get user info internal request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [uint64](#uint64) |  | User ID |






<a name="users-UserInfoInternalResponse"></a>

### UserInfoInternalResponse
Get user info internal response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| data | [UserData](#users-UserData) |  | User data |





 

 

 


<a name="users-UsersInternalService"></a>

### UsersInternalService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| UserInfo | [UserInfoInternalRequest](#users-UserInfoInternalRequest) | [UserInfoInternalResponse](#users-UserInfoInternalResponse) | Get user info Returns error codes: - InvalidArgument - for bad request - NotFound - if user not found - Internal - on internal error |

 



<a name="src_userspb_public-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## src/userspb/public.proto



<a name="users-ForgotPasswordRequest"></a>

### ForgotPasswordRequest
Send reset password link request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| email | [string](#string) |  | User&#39;s email |






<a name="users-RefreshTokensRequest"></a>

### RefreshTokensRequest
Refresh JWT-tokens request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| refresh_token | [string](#string) |  | Current user&#39;s refresh JWT-token |






<a name="users-RefreshTokensResponse"></a>

### RefreshTokensResponse
Refresh JWT-tokens response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| access_token | [string](#string) |  | New access JWT-token |
| refresh_token | [string](#string) |  | New refresh JWT-token |






<a name="users-ResetPasswordRequest"></a>

### ResetPasswordRequest
Reset password request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| token | [string](#string) |  | Secret token |
| password | [string](#string) |  | New password |






<a name="users-SignInRequest"></a>

### SignInRequest
SignIn request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| email | [string](#string) |  | Email |
| password | [string](#string) |  | Password |






<a name="users-SignInResponse"></a>

### SignInResponse
SignIn response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| access_token | [string](#string) |  | Access JWT-token |
| refresh_token | [string](#string) |  | Refresh JWT-token |






<a name="users-SignUpRequest"></a>

### SignUpRequest
SignUp request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| email | [string](#string) |  | Email |
| name | [string](#string) |  | User&#39;s name |
| last_name | [string](#string) |  | User&#39;s last name |
| password | [string](#string) |  | Password |






<a name="users-SignUpResponse"></a>

### SignUpResponse
SignUp response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_uuid | [string](#string) |  | User&#39;s UUID |






<a name="users-UpdateUserRequest"></a>

### UpdateUserRequest
Update user info request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| access_token | [string](#string) |  | User&#39;s access JWT-token |
| name | [string](#string) | optional | User name |
| last_name | [string](#string) | optional | User last name |






<a name="users-UserInfoRequest"></a>

### UserInfoRequest
Get user info request


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| access_token | [string](#string) |  | User&#39;s access JWT-token |






<a name="users-UserInfoResponse"></a>

### UserInfoResponse
Get user info response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| data | [UserData](#users-UserData) |  | User data |





 

 

 


<a name="users-UsersPublicService"></a>

### UsersPublicService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| SignUp | [SignUpRequest](#users-SignUpRequest) | [SignUpResponse](#users-SignUpResponse) | Create user Returns error codes: - InvalidArgument - for bad request - Internal - on internal error |
| SignIn | [SignInRequest](#users-SignInRequest) | [SignInResponse](#users-SignInResponse) | Generate auth JWT-tokens Returns error codes: - InvalidArgument - for bad request - Unauthenticated - for wrong credentials - Internal - on internal error |
| RefreshTokens | [RefreshTokensRequest](#users-RefreshTokensRequest) | [RefreshTokensResponse](#users-RefreshTokensResponse) | Generate new access and refresh tokens by old refresh token Returns error codes: - InvalidArgument - for bad request - Unauthenticated - if token is invalid - Internal - on internal error |
| UpdateUser | [UpdateUserRequest](#users-UpdateUserRequest) | [.google.protobuf.Empty](#google-protobuf-Empty) | Update user info Returns error codes: - InvalidArgument - for bad request - Unauthenticated - if token is invalid or user not found - Internal - on internal error |
| UserInfo | [UserInfoRequest](#users-UserInfoRequest) | [UserInfoResponse](#users-UserInfoResponse) | Get user info Returns error codes: - InvalidArgument - for bad request - Unauthenticated - if token is invalid or user not found - Internal - on internal error |
| ForgotPassword | [ForgotPasswordRequest](#users-ForgotPasswordRequest) | [.google.protobuf.Empty](#google-protobuf-Empty) | Send reset password link to user&#39;s email Returns error codes: - InvalidArgument - for invalid request - NotFound - if user not found by email - Internal - on internal error |
| ResetPassword | [ResetPasswordRequest](#users-ResetPasswordRequest) | [.google.protobuf.Empty](#google-protobuf-Empty) | Set new password Returns error codes: - InvalidArgument - for invalid request - Unauthenticated - if token is invalid or user not found - Internal - on internal error |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

