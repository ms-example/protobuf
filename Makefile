ifndef ROOT
	ROOT = $(PWD)
endif

go.image.build:
	@echo "-> build go image"
	@docker build -f ./console/go/Dockerfile ./console/go -t mse_proto_console_go

GOREPS=\
	userspb \
	notifierpb

.PHONY: $(GOREPS)
$(GOREPS): go.image.build
	@echo "-> build $@"
	@mkdir -p "$@"
	@docker run -v $(ROOT)/:/proto-build --name mse_proto_console_go --rm -it mse_proto_console_go sh -c "\
		protoc \
			--go_out=./$@/ \
			--go_opt=paths=source_relative \
			--go-grpc_out=./$@/ \
			--go-grpc_opt=paths=source_relative \
			--openapiv2_out=logtostderr=true:./$@ \
			--openapiv2_opt=output_format=json \
			--grpc-gateway_out=./$@/ \
			--grpc-gateway_opt=logtostderr=true \
			--grpc-gateway_opt=paths=source_relative \
			--doc_out=./$@/ \
			--doc_opt=markdown,$@.md \
			--experimental_allow_proto3_optional \
			./src/$@/*.proto;"
	@mv $@/src/$@/* $@/
	@rm -rf $@/src
